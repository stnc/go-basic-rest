package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/rest_api_example")
	if err != nil {
		fmt.Print(err.Error())
	}
	defer db.Close()
	// make sure connection is available
	err = db.Ping()
	if err != nil {
		fmt.Print(err.Error())
	}

	stmt1, err := db.Prepare("CREATE DATABASE rest_api_example;")
	if err != nil {
		fmt.Println(err.Error())
	}

	_, err = stmt1.Exec()
	if err != nil {
		fmt.Print(err.Error())
	} else {
		fmt.Printf("rest_api_example  successfully migrated....")
	}

	stmt, err := db.Prepare("CREATE TABLE users  (id int NOT NULL AUTO_INCREMENT, name  varchar(40), age  INT NOT NULL, PRIMARY KEY (id));")
	if err != nil {
		fmt.Println(err.Error())
	}
	_, err = stmt.Exec()
	if err != nil {
		fmt.Print(err.Error())
	} else {
		fmt.Printf("users Table successfully migrated....")
	}
}
